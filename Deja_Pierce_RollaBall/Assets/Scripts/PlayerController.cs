﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text CountText;
    public Text WinText;
    public Text ScoreText;

    private Rigidbody rb;
    private int count;

    //Player's Material
    Material playerMat;

    //Player's Original Color
    Color originalColor;



    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        WinText.text = "";

        //Saved Reference to the ball's material
        playerMat = GetComponent<Renderer>().material;

        //What the color of the player is at the start , before it flashes different colors
        originalColor = playerMat.color;
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    private void OnDestroy()
    {
        //Collects the material that was made
        Destroy(playerMat);
    }

    private void Update()
    {
        // Sets the player's color
        playerMat.color = Color.Lerp(playerMat.color, originalColor, Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();

            playerMat.color = Random.ColorHSV();
        }
    }
    void SetCountText()
    {
        CountText.text = "Count:" + count.ToString();
        if (count >= 11)
        {
            WinText.text = "You Win!";
        }
    }

  
}
