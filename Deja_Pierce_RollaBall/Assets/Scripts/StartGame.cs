﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGame : MonoBehaviour
{
    // Creating Countdown timer
    // Start is called before the first frame update

    // how long the coutdown is 
    // countdown will start at 3
    public float timeLeft = 3.0f;
    public Text startText;
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {

        // when the timer will end, what will be displayed when the countdown ends
        timeLeft -= Time.deltaTime;
        startText.text = (timeLeft).ToString("0");
        if (timeLeft <= 0f )
        {

            // when the countdown is over a 0 will be displayed 
            timeLeft = 0;
        

            // Couldn't figure out how to start countdown before the timer starts, so I changed the timer to start at -3.00 seconds
        }
    }
}
